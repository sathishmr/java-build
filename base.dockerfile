FROM ubuntu:16.04

LABEL maintainer="Kirk Rader <apps@rader.us>"

LABEL  description="Dockerfile for a development environment image \
common to a team of users and their shared CI/CD build server"

LABEL copyright="Copyright 2018 Kirk Rader"

LABEL license="Licensed under the Apache License, Version 2.0 (the \
&quot;License&quot;); you may not use this file except in compliance with the \
License. You may obtain a copy of the License at \
http://www.apache.org/licenses/LICENSE-2.0 \ Unless required by applicable law \
or agreed to in writing, software distributed under the License is distributed \
on an &quot;AS IS&quot; BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, \
either express or implied. See the License for the specific language governing \
permissions and limitations under the License."

# Note: this Dockerfile should contain only the base development environment
# configuration. Put any user-specific configuration into user.dockerfile

# Update packages:
RUN apt-get update && apt-get -y upgrade

# Install basic admin packages
RUN apt-get -y install apt-utils
RUN apt-get -y install vim whois sudo net-tools

# Install basic development libraries and tools for the underlying platform:
RUN apt-get -y install build-essential libtool autoconf automake bison flex git

RUN apt-get -y install curl software-properties-common
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
RUN apt-get -y install git-lfs

# Install Java development tools:
RUN apt-get -y install openjdk-8-jdk maven

# Install additional tools to support rich documentation:
RUN apt-get -y install ghostscript graphviz doxygen
RUN apt-get -y install texlive-full
RUN mkdir -p /var/opt/plantuml
COPY plantuml.jar /var/opt/plantuml
ENV PLANTUML_JAR /var/opt/plantuml/plantuml.jar
ENV GRAPHVIZ_DOT /usr/bin/dot
ENV DOXYGEN_EXE /usr/bin/doxygen

RUN apt-get -y autoremove
