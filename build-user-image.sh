#!/bin/bash

# Copyright 2018 Kirk Rader
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Build a user-specific image from user.dockerfile

# Usage: ./build-user-image.sh [ <password> [ <userid> ] ]

function ensuredirs() {
  mkdir -p ssh
  mkdir -p m2
  touch ssh/build.txt
  touch m2/build.txt
}

function cleanup() {
  if [ -d ssh ]; then rm -rf ssh; fi
  if [ -d m2 ]; then rm -rf m2; fi
}

# Check for password parameter:
if [ -z "$1" ]; then
  echo -n "Password: "
  read -s PASSWORD
  echo
else
  PASSWORD="$1"
fi

# Check for userid parameter:
if [ -z "$2" ]; then
  # If userid not supplied, clone current user's environment in image
  USERNAME=$USER
  trap cleanup EXIT ERR
  if [ ! -z "$(command -v git)" ]; then
    FULLNAME="$(git config user.name)"
    EMAIL="$(git config user.email)"
  fi
  if [ -z "$FULLNAME" ]; then
    echo -n "Full name: "
    read FULLNAME
  fi
  if [ -z "$EMAIL" ]; then
    echo -n "Email: "
    read EMAIL
  fi
  if [ -d ssh ]; then rm -rf ssh; fi
  if [ -d m2 ]; then rm -rf m2; fi
  ensuredirs
  if [ -d $HOME/.ssh ]; then cp -r $HOME/.ssh/* ssh; fi
  if [ -d $HOME/.m2 ]; then cp $HOME/.m2/settings*.xml m2; fi
else
  # Otherwise, create server environment
  USERNAME=$2
  echo -n "Full name: "
  read FULLNAME
  echo -n "Email: "
  read EMAIL
  ensuredirs
fi

# Build the image:
docker build -t $USERNAME-java-build --force-rm \
  --build-arg USERNAME=$USERNAME \
  --build-arg PASSWORD="$PASSWORD" \
  --build-arg FULLNAME="$FULLNAME" \
  --build-arg EMAIL="$EMAIL" \
  --file user.dockerfile \
  .
