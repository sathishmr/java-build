FROM parasaurolophus/java-build:latest

LABEL maintainer="Kirk Rader <apps@rader.us>"

LABEL  description="Dockerfile for a development environment image \
used by a specific user or CI/CD build server"

LABEL copyright="Copyright 2018 Kirk Rader"

LABEL license="Licensed under the Apache License, Version 2.0 (the \
&quot;License&quot;); you may not use this file except in compliance with the \
License. You may obtain a copy of the License at \
http://www.apache.org/licenses/LICENSE-2.0 \ Unless required by applicable law \
or agreed to in writing, software distributed under the License is distributed \
on an &quot;AS IS&quot; BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, \
either express or implied. See the License for the specific language governing \
permissions and limitations under the License."

# Declare GitLab CI intermediate directories
VOLUME /builds
VOLUME /cache

# Run a bash shell by default:
CMD ["bash"]
