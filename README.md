_Copyright &copy; 2018 Kirk Rader_

_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at_

> <http://www.apache.org/licenses/LICENSE-2.0>

_Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._

# Containerized Java Build Environment

> **Note:** when making changes to [base.dockerfile](base.dockerfile) or [user.dockerfile](user.dockerfile) it is important to remember to keep all features that should be common to all users and the CI/CD build server in [base.dockerfile](base.dockerfile) while only [user.dockerfile](user.dockerfile) should contain sensitive, user-specific information like user names, SSH keys etc.

## Summary

Following procedures similar to those described [below](#usage), every user will have access to a containerized build environment identical to one another's and to that used on the CI/CD server.

- DevOps uses [build-base-image.sh](build-base-image.sh) to create a common, shared build environment image

- Users use [build-user-image.sh](build-user-image.sh) to create personal images customized with the appropriate SSH keys, Maven settings etc.

- DevOps can also use [build-user-image.sh](build-user-image.sh) to create images for use on CI/CD servers and the like

- [build-base-image.sh](build-base-image.sh) uses [base.dockerfile](base.dockerfile) to build an image suitable for pushing to a shared repository

- [build-user-image.sh](build-user-image.sh) uses [user.dockerfile](user.dockerfile) to create images customized for a particular user or CI/CD process

- [run-container.sh](run-container.sh) provides a convenience wrapper for creating and running containers from the current user's personal image

- [user.dockerfile](user.dockerfile) and [run-container.sh](run-container.sh) support mapping file system directories into the user's container

    - _/projects_ mount point to hold source file working directories, built artifacts etc.

    - _/mvn_repo_ mount point to store the local Maven repository

## Overview

| File                                      | Description                      |
| ----------------------------------------- | -------------------------------- |
| [base.dockerfile](base.dockerfile)        | Dockerfile for base image containing standard build tool chain (Java 8, Maven, Doxygen etc.) |
| [user.dockerfile](user.dockerfile)         | Dockerfile for creating images customized for an individual user |
| [build-base-image.sh](build-base-image.sh) | Convenience script for building _parasaurolophus/java-build_ image from [base.dockerfile](base.dockerfile) |
| [build-user-image.sh](build-user-image.sh) | Convenience script for building a particular user's image from [user.dockerfile](user.dockerfile) |
| [run-container.sh](run-container.sh) | Convenience script for running containers from images created using [build-user-image.sh](build-user-image.sh) |
| clean.sh                                   | Convenience script to prune dangling containers, images and volumes |

The preceding scripts and Docker configuration files can be used to produce containers like:

```mermaid
graph BT

  subgraph Shared Docker Repo

    baseimage[java-build image]

  end

  subgraph dev1 Workstation

    subgraph dev1 Docker Engine

      dev1image[dev1-java-build image]
      dev1container[dev1-java-build container]

      dev1container-->|run-container.sh|dev1image

    end

    subgraph dev1 File System

      projects1[dev1<br>projects<br>directory]
      mvnrepo1[dev1<br>Maven<br>repository]

    end

  end

  subgraph CI/CD Server

    subgraph CI/CD Docker Engine

      serverimage[server-java-build image]
      servercontainer[server-java-build container]

      servercontainer-->|run-server-container.sh|serverimage

    end

    subgraph CI/CD Server File System

      projects3[server<br>projects<br>directory]
      mvnrepo3[server<br>Maven<br>repository]

    end

  end

  subgraph dev2 Workstation

    subgraph dev2 Docker Engine

      dev2image[dev2-java-build image]
      dev2container[dev2-java-build container]

      dev2container-->|run-container.sh|dev2image

    end

    subgraph dev2 File System

      projects2[dev2<br>projects<br>directory]
      mvnrepo2[dev2<br>Maven<br>repository]

    end

  end

  dev1image-->|FROM java-build|baseimage
  serverimage-->|FROM java-build|baseimage
  dev2image-->|FROM java-build|baseimage
  projects1---|VOLUME /projects|dev1container
  projects3---|VOLUME /projects|servercontainer
  projects2---|VOLUME /projects|dev2container
  mvnrepo1---|VOLUME /mvn_repo|dev1container
  mvnrepo3---|VOLUME /mvn_repo|servercontainer
  mvnrepo2---|VOLUME /mvn_repo|dev2container
```

where each container contains a common build environment with user- or server-specific credentials and similar configuration.

The [base.dockerfile](base.dockerfile) defines the following base build environment:

- Ubuntu 16.04
    - build-essentials
    - libtool
    - autoconf
    - automake
- Git (with LFS)
- OpenJDK 8
- Maven
- Doxygen
- Graphviz
- PlantUML

The [user.dockerfile](user.dockerfile) adds to the image created by [base.dockerfile](base.dockerfile) by:

- Logging into Ubuntu as a specific user
- Including that user's SSH keys in _~/.ssh_
- Including that user's Maven _settings.xml_ in _~/.m2_
- Configuring Git with full name and email address (`git config --global ...`)
- Defining mount points, _/projects_ and _/mvn_repo_, where build scripts can find source files, create and install build artifacts etc. in the host file system

## Usage

These instructions assume two roles:

| Role   | Responsibilities                                          | Permissions |
| ------ | --------------------------------------------------------- | ----------- |
| DevOps | Maintain and operate build, test and runtime environments | Full administrator control over shared source control repositories, Docker image repositories etc. |
| User   | Writes code, authors test automation, runs unit and integration tests | Pull _java-build_ image from repository |

### DevOps

#### Base Image

The [build-base-image.sh](build-base-image.sh) script provides a simple convenience wrapper for invoking `docker build ...` to create an image from [base.dockerfile](base.dockerfile):

```bash
./build-base-image.sh
push parasaurolophus/java-build:latest

mkdir ssh
ssh-keygen -o -t rsa -C "ciciduserid@yourdomain.com" -b 4096 -f ssh/id_rsa
# ...be sure to copy this key pair into the CI/CD server's source control repository...
# ...create additional SSH configuration files as appropriate...

mkdir m2
# ...populate m2/settings.xml appropriately...

./build-user-image cicdpassword ciciduserid

# to test the container...
docker volume create projects
docker volume create mvn_repo
docker run --rm -it -v projects:/projects -v mvn_repo:/mvn_repo cicduserid-java-build
```

- Replace _cicidpassword_ and _ciciduserid_ with desired credentials for this special image

- You will be prompted for full name and email address to use when configuring Git in the image

- You should then configure your CI/CD server to use the custom user image you just created is an invocation of `docker run ...` similar to that shown in the preceding examples

> **Note:** change the image tag in the various scripts and configuration files from _parasaurolophus_ to your own Docker Hub or other image repository tag if you fork this source repository.

#### CI/CD Server

DevOps can use [build-user-image.sh](build-user-image.sh), described in the following section, to create images for use by the CI/CD server process by specifying an alternate user id as a second parameter.

### Users

Once DevOps has pushed _parasaurolophus/java-build_ to a shared image repository, [build-user-image.sh](build-user-image.sh) provides a convenience wrapper for `docker build ...` to create an image from [user.dockerfile](user.dockerfile):

```bash
git config --global user.name "Your Full Name"
git config --global user.email "yourid@yourdomain.com"

# after DevOps has pushed parasaurolophus/java-build:latest...
./build-user-image.sh
# note: this will create an image containing sensitive information including SSH keys, email address etc. so it should never be pushed to a shared repository

./run-container.sh
```

- You will be prompted for the password you would like to use in your image

- Full name and email address will be copied from your current Git configuration

- _~/.ssh_ and _~/.m2_ will be copied from your host file-system home directory

## Theory of Operation

```bash
./build-base-image.sh
```

[build-base-image.sh](build-base-image.sh) takes no parameters. It is simply a convenience wrapper for building and tagging an image using [base.dockerfile](base.dockerfile).

```bash
./build-user-image.sh [ password [ userid ] ]
```

When invoked with zero or one parameter, [build-user-image.sh](build-user-image.sh) will attempt to clone the current user's identity and configuration into the image being created using [user.dockerfile](user.dockerfile).

When invoked with the second optional parameter, [build-user-image.sh](build-user-image.sh) will create an environment independent of the current user's identity.

> **Assumption:** Individual users will invoke [build-user-image.sh](build-user-image.sh) for themselves without passing a second parameter. DevOps will use the two-parameter invocation to create images for special purposes, such as for use on a CI/CD server.

In either case, when [build-user-image.sh](build-user-image.sh) is not supplied and cannot deduce values for the following values it will prompt the user for them:

| Prompt    | Value                                          |
| --------- | ---------------------------------------------- |
| Password  | Password for the user in the image             |
| Full name | Value for `git config --global user.name ...`  |
| Email     | Value for `git config --global user.email ...` |

When invoked with zero or one parameter, [build-user-image.sh](build-user-image.sh) will copy the current user's _~/.ssh_ to _ssh/_ and _~/.m2_ to _m2/_. When passed a second parameter, [build-user-image.sh](build-user-image.sh) will assume that _ssh/_ and _m2/_ already exist and contain the desired SSH and Maven key and configuration files, respectively. There are corresponding `COPY` directives in _user.dockerfiles_. The end result is populating the specified user's SSH and Maven settings in the image.

```mermaid
graph TB

  start((start))
  passwordsupplied{password<br>parameter<br>supplied?}
  promptpassword[prompt for password]
  setpassword[set password]
  useridsupplied{userid<br>parameter<br>supplied?}
  promptfullname1[prompt for full name]
  setfullname1[set full name]
  promptemail1[prompt for email]
  setemail1[set email]
  copyuserid[set user id from $USER]
  getfullname{get full<br>name from<br>Git?}
  promptfullname2[prompt for full name]
  setfullname2[set full name]
  promptemail2[prompt for email]
  setemail2[set email]
  getemail{get email<br>from Git?}
  copyssh["copy ~/.ssh to ssh/"]
  copym2["copy ~/.m2/settings.xml to m2/settings.xml"]
  settrap[set trap to clean up ssh/ and m2/]
  assumedirs>assume ssh/ and m2/ directories already exist]
  buildimage[invoke Docker to build image using user.dockerfile]
  trapset{trap set?}
  cleanup[clean up ssh/ and m2/]
  stop((stop))

  start-->passwordsupplied
  passwordsupplied-->|no|promptpassword
  passwordsupplied-->|yes|setpassword
  promptpassword-->setpassword
  setpassword-->useridsupplied
  useridsupplied-->|yes|settrap
  settrap-->promptfullname1
  promptfullname1-->setfullname1
  setfullname1-->promptemail1
  promptemail1-->setemail1
  useridsupplied-->|no|copyuserid
  copyuserid-->getfullname
  getfullname-->|no|promptfullname2
  promptfullname2-->setfullname2
  getfullname-->|yes|setfullname2
  setfullname2-->getemail
  getemail-->|no|promptemail2
  promptemail2-->setemail2
  getemail-->|yes|setemail2
  setemail2-->copyssh
  copyssh-->copym2
  copym2-->buildimage
  setemail1-->assumedirs
  assumedirs-->buildimage
  buildimage-->trapset
  trapset-->|yes|cleanup
  trapset-->|no|stop
  cleanup-->stop
```

> **Warning:** [build-user-image.sh](build-user-image.sh) creates an Ubuntu user with your personal user name, full name, email address and SSH keys. You should never push such an image to a shared repository.

```bash
./run-container.sh
```

The invocation of [build-user-image.sh](build-user-image.sh) described above will create an image in your local Docker engine with a tag like _youruserid-java-build:latest_. Use the [run-container.sh](run-container.sh) convenience wrapper for `docker run ...` that invokes a container created from your personal image.

> **Note:** [run-container.sh](run-container.sh) maps _~/projects_ in your host file system to the _/projects_ volume in your container. Build scripts should look for source files and place artifacts under _/projects_ which you can then access using native GUI based tools under _~/projects_. Similarly, _~/.m2/repository_ is mapped to _/mvn_repo_ which, in turn, is specified to be used by Maven as its local repository. Use the Docker Engine file sharing UI to make those directories available to containers.

### Overall Work Flow

```mermaid
sequenceDiagram

  participant DevOps
  participant scm as Source Control Server
  participant docker1 as DevOps Docker Engine
  participant imagerepo as Team Docker Repository
  participant docker2 as CI/CD Docker Engine
  participant cicd as CI/CD Server
  participant docker3 as User Docker Engine
  participant User

  DevOps->>scm: pull build-base-image.sh and base.dockerfile
  DevOps->>+docker1: run build-base-image.sh
  docker1->>-docker1: build java-build
  DevOps->>imagerepo: push java-build

  DevOps->>scm: pull build-user-image.sh and user.dockerfile
  DevOps->>+docker2: run build-user-image.sh
  docker2->>-docker2 : build server-java-build image
  loop CI/CD
    cicd ->> docker2 : use run-server-container.sh
  end

  User->>scm: pull build-user-image.sh, user.dockerfile and run-container.sh
  User->>+docker3: run build-user-image.sh
  docker1->>docker3: pull java-build image
  docker3->>-docker3 : build user-java-build image
  loop edit-compile-test
    User->>docker3: use run-container.sh to build
  end
```
