FROM parasaurolophus/java-build:latest

LABEL maintainer="Kirk Rader <apps@rader.us>"

LABEL  description="Dockerfile for a development environment image \
used by a specific user or CI/CD build server"

LABEL copyright="Copyright 2018 Kirk Rader"

LABEL license="Licensed under the Apache License, Version 2.0 (the \
&quot;License&quot;); you may not use this file except in compliance with the \
License. You may obtain a copy of the License at \
http://www.apache.org/licenses/LICENSE-2.0 \ Unless required by applicable law \
or agreed to in writing, software distributed under the License is distributed \
on an &quot;AS IS&quot; BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, \
either express or implied. See the License for the specific language governing \
permissions and limitations under the License."

# Note: this Dockerfile is used to extend an image created using base.dockerfile
# with user- (or build-server-) specific user names, SSH keys etc.

# The user name for logging into Linux:
ARG USERNAME

# The user password:
ARG PASSWORD

# The value for git config user.name ...
ARG FULLNAME

# The value for git config user.email ...
ARG EMAIL

# The user's home directory
ENV HOMEDIR /home/$USERNAME

# Put all source control working directories, build output directories etc. here:
VOLUME /projects

# Shared Maven repository
VOLUME /mvn_repo

# Create the user:
RUN echo "$USERNAME:$PASSWORD::::/home/$USERNAME:/bin/bash" > newusers.txt
RUN newusers newusers.txt
RUN rm newusers.txt

# Copy the SSH keys and configuration:
COPY ssh $HOMEDIR/.ssh
RUN chmod 755 $HOMEDIR/.ssh
RUN chmod -R 600 $HOMEDIR/.ssh/*
RUN find $HOMEDIR/.ssh -name \*.pub -exec chmod 644 \{\} \;
RUN chown -R $USERNAME:$USERNAME $HOMEDIR/.ssh

# Copy Maven settings:
COPY m2 $HOMEDIR/.m2
RUN chmod -R 700 $HOMEDIR/.m2
RUN chown -R $USERNAME:$USERNAME $HOMEDIR/.m2

# Grant sudo privilege
RUN usermod -aG sudo $USERNAME

# Global Maven configuration:
RUN mv /etc/maven/settings.xml /etc/maven/settings.xml.orig
COPY global-maven-settings.xml /etc/maven/settings.xml

# Run as the specified user:
USER $USERNAME
WORKDIR $HOMEDIR

# Configure Git:
RUN git config --global user.name "$FULLNAME"
RUN git config --global user.email "$EMAIL"
RUN git lfs install

# Run a bash shell by default:
CMD ["bash"]
