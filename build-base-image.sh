#!/bin/bash

# Copyright 2018 Kirk Rader
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Build java-build image from base.dockerfile

# Usage: ./build-base-image.sh

# Build the image:
docker build -t parasaurolophus/java-build:latest --force-rm --file base.dockerfile .
docker tag parasaurolophus/java-build:latest registry.gitlab.com/kirkrader/java-build:latest
